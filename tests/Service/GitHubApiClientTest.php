<?php

namespace App\Tests\Service;

use App\Dto\GitHubUserDto;
use App\Tests\BaseKernelTestCase;
use App\Tests\TestUtil\GitHubApiClientFactory;

/**
 * @property array testCases
 */
class GitHubApiClientTest extends BaseKernelTestCase
{
    private static GitHubApiClientFactory $factory;
    private static array $testCases;

    public static function setUpBeforeClass(): void
    {
        static::$factory = new GitHubApiClientFactory();
        static::$testCases = static::getTestCases('/data/api_client_cases.json');
    }

    public function testGetUserSuccess()
    {
        $user = static::$factory->create(json_encode(static::$testCases['user_success']))
            ->getUser('test');

        $this->assertInstanceOf(GitHubUserDto::class, $user);
        $this->assertEquals(1, $user->getId());
        $this->assertEquals('test', $user->getUsername());
        $this->assertEquals('Test Test', $user->getName());
        $this->assertEquals(10, $user->getPublicRepos());
        $this->assertEquals('http://test-blog.com/', $user->getBlog());
        $this->assertObjectNotHasAttribute('extrafield', $user);
    }

    public function testGetUserSuccessWithNoName()
    {
        $user = static::$factory->create(json_encode(static::$testCases['user_success_with_no_name']))
            ->getUser('test');

        $this->assertInstanceOf(GitHubUserDto::class, $user);
        $this->assertEquals(2, $user->getId());
        $this->assertEquals('test1', $user->getUsername());
        $this->assertEquals('test1', $user->getName());
        $this->assertEquals(12, $user->getPublicRepos());
    }

    public function testGetUserSuccessWithOnlyRequiredFields()
    {
        $user = static::$factory->create(json_encode(static::$testCases['user_success_only_required_fields']))
            ->getUser('test2');

        $this->assertInstanceOf(GitHubUserDto::class, $user);
        $this->assertEquals(3, $user->getId());
        $this->assertEquals('test2', $user->getUsername());
        $this->assertEquals('test2', $user->getName());
        $this->assertEquals(0, $user->getPublicRepos());
        $this->assertNull($user->getBlog());
    }

    public function testGetUserReturnsNullWithEmptyBody()
    {
        $user = static::$factory->create('{}')
            ->getUser('-');

        $this->assertNull($user);
    }

    public function testGetUserReturnsNullWithNotAllRequiredField()
    {
        $user = static::$factory->create('{"login":"test3"}')
            ->getUser('test3');

        $this->assertNull($user);
    }

    public function testGetRepoSuccess()
    {
        $repos = static::$factory->create(json_encode(static::$testCases['repos_success']))
            ->getRepos('test');

        $this->assertIsArray($repos);
        $this->assertCount(2, $repos);
        $this->assertEquals(1, $repos[0]->getId());
        $this->assertEquals('test', $repos[0]->getName());
        $this->assertEquals('test/test', $repos[0]->getFullName());
        $this->assertEquals(6, $repos[0]->getForks());
        $this->assertEquals(22, $repos[0]->getPopularity());
    }

    public function testGetRepoSuccessWithOnlyRequiredFields()
    {
        $repos = static::$factory->create(json_encode(static::$testCases['repos_success_only_required_fields']))
            ->getRepos('test');

        $this->assertIsArray($repos);
        $this->assertCount(1, $repos);
        $this->assertEquals(3, $repos[0]->getId());
        $this->assertEquals('test2', $repos[0]->getName());
        $this->assertEquals('https://api.github.com/repos/test2/test2', $repos[0]->getUrl());
        $this->assertNull($repos[0]->getFullName());
        $this->assertEquals(0, $repos[0]->getForks());
        $this->assertEquals(0, $repos[0]->getPopularity());
    }

    public function testGetRepoReturnsEmptyArray()
    {
        $repos = static::$factory->create('{}')->getRepos('test');

        $this->assertIsArray($repos);
        $this->assertCount(0, $repos);
    }
}

<?php

namespace App\Tests\TestUtil;

use App\Service\GitHubApiClient;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Serializer\SerializerInterface;

class GitHubApiClientFactory extends KernelTestCase
{
    /**
     * @param string $content json
     */
    public function create(?string $content = null, ?ClientInterface $client = null, ?SerializerInterface $serializer = null): GitHubApiClient
    {
        if (!$client) {
            $client = $this->createClientInterface((string) $content);
        }

        if (!$serializer) {
            self::bootKernel();
            $serializer = static::getContainer()->get(SerializerInterface::class);
        }

        return new GitHubApiClient($client, $serializer);
    }

    /**
     * @param string $content json
     */
    private function createClientInterface(string $content, int $statusCode = 200): ClientInterface
    {
        $body = $this->createMock(StreamInterface::class);
        $body
            ->method('getContents')
            ->willReturn($content);

        $response = $this->createMock(ResponseInterface::class);
        $response
            ->method('getBody')
            ->willReturn($body);

        $response
            ->method('getStatusCode')
            ->willReturn($statusCode);

        $client = $this->createMock(ClientInterface::class);
        $client
            ->method('request')
            ->willReturn($response);

        return $client;
    }
}

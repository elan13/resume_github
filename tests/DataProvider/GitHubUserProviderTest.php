<?php

namespace DataProvider;

use App\DataProvider\GitHubUserProvider;
use App\Dto\GitHubUserDto;
use App\Tests\BaseKernelTestCase;
use App\Tests\TestUtil\GitHubApiClientFactory;
use InvalidArgumentException;

class GitHubUserProviderTest extends BaseKernelTestCase
{
    private static GitHubApiClientFactory $factory;
    private static array $testCases;

    public static function setUpBeforeClass(): void
    {
        static::$factory = new GitHubApiClientFactory();
        static::$testCases = static::getTestCases('/data/api_client_cases.json');
    }

    public function testGetItemSuccess()
    {
        $client = static::$factory->create(json_encode(static::$testCases['user_success']));

        $user = (new GitHubUserProvider($client))->getItem('test');

        $this->assertInstanceOf(GitHubUserDto::class, $user);
        $this->assertEquals(1, $user->getId());
        $this->assertEquals('test', $user->getUsername());
        $this->assertEquals('Test Test', $user->getName());
        $this->assertEquals(10, $user->getPublicRepos());
        $this->assertEquals('http://test-blog.com/', $user->getBlog());
        $this->assertObjectNotHasAttribute('extrafield', $user);
    }

    public function testGetUserReturnsNullWithNotAllRequiredField()
    {
        $client = static::$factory->create('{"login":"test3"}');
        $user = (new GitHubUserProvider($client))->getItem('test');

        $this->assertNull($user);
    }

    public function testGetItemExpectExceptionWithEmptyUsername()
    {
        $this->expectException(InvalidArgumentException::class);
        $client = static::$factory->create('{}');
        (new GitHubUserProvider($client))->getItem('');
    }

    public function testGetItemExpectExceptionWithUsernameAsArray()
    {
        $this->expectException(InvalidArgumentException::class);
        $client = static::$factory->create('{}');
        (new GitHubUserProvider($client))->getItem([]);
    }
}

<?php

namespace DataProvider;

use App\DataProvider\GitHubRepoInfoProvider;
use App\DataProvider\GitHubUserProvider;
use App\Tests\BaseKernelTestCase;
use App\Tests\TestUtil\GitHubApiClientFactory;
use InvalidArgumentException;

class GitHubRepoInfoProviderTest extends BaseKernelTestCase
{
    private static GitHubApiClientFactory $factory;
    private static array $testCases;

    public static function setUpBeforeClass(): void
    {
        static::$factory = new GitHubApiClientFactory();
        static::$testCases = static::getTestCases('/data/api_client_cases.json');
    }

    public function testGetItemExpectExceptionWithEmptyUsername()
    {
        $this->expectException(InvalidArgumentException::class);
        $client = static::$factory->create('{}');
        (new GitHubUserProvider($client))->getItem('');
    }

    public function testGetItemExpectExceptionWithUsernameAsArray()
    {
        $this->expectException(InvalidArgumentException::class);
        $client = static::$factory->create('');
        (new GitHubUserProvider($client))->getItem([]);
    }

    public function testGetItemSuccess()
    {
        $client = static::$factory->create(json_encode(static::$testCases['repos_success']));
        $repoInfo = (new GitHubRepoInfoProvider($client))->getItem('test');

        $repos = $repoInfo->getRepos();
        $languages = $repoInfo->getLanguages();

        $this->assertEquals('test', $repoInfo->getUsername());
        $this->assertIsArray($repos);
        $this->assertCount(2, $repos);

        $this->assertEquals(2, $repos[0]->getId());
        $this->assertEquals('test1', $repos[0]->getName());
        $this->assertEquals(2, $repos[0]->getId());
        $this->assertEquals(147, $repos[0]->getPopularity(), 'Sorting failed; 147 > 22');

        $this->assertEquals('test', $repos[1]->getName());
        $this->assertEquals('test/test', $repos[1]->getFullName());
        $this->assertEquals(6, $repos[1]->getForks());
        $this->assertEquals(22, $repos[1]->getPopularity(), 'Sorting failed; 147 > 22');

        $this->assertIsArray($languages);
        $this->assertCount(2, $languages);

        $this->assertEquals('JavaScript', $languages[0]->getLanguageName(), 'Sorting failed; 93.0 > 7.0');
        $this->assertEquals(4405, $languages[0]->getUsageSize());
        $this->assertEquals(93.0, $languages[0]->getPercent(), 'Percent was calculated incorrectly');

        $this->assertEquals('C++', $languages[1]->getLanguageName());
        $this->assertEquals(333, $languages[1]->getUsageSize());
        $this->assertEquals(7.0, $languages[1]->getPercent(), 'Percent was calculated incorrectly');
    }

    public function testGetRepoReturnsEmptyArray()
    {
        $client = static::$factory->create('{}');
        $repoInfo = (new GitHubRepoInfoProvider($client))->getItem('test');

        $repos = $repoInfo->getRepos();
        $languages = $repoInfo->getLanguages();

        $this->assertIsArray($repos);
        $this->assertCount(0, $repos);

        $this->assertIsArray($languages);
        $this->assertCount(0, $languages);
    }
}

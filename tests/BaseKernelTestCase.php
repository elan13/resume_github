<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BaseKernelTestCase extends KernelTestCase
{
    public static function getTestCases(string $relativeFilePath): array
    {
        $content = file_get_contents(__DIR__.$relativeFilePath);

        if (empty($content)) {
            throw new \LogicException(sprintf('"%s" file does not exist', $relativeFilePath));
        }

        $cases = json_decode($content, true);
        if (empty($cases)) {
            throw new \LogicException(sprintf('"%s" file contains invalid data', $relativeFilePath));
        }

        return $cases;
    }
}

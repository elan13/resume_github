<?php

namespace App\Service;

use App\Dto\GitHubRepoDto;
use App\Dto\GitHubUserDto;
use Symfony\Component\HttpFoundation\Response;

class GitHubApiClient extends AbstractApiClient
{
    public function getUser(string $username): ?GitHubUserDto
    {
        $endpoint = sprintf('users/%s', $username);
        $response = $this->client->request('GET', $endpoint);

        if (Response::HTTP_OK !== $response->getStatusCode()) {
            $this->logging($endpoint, $response);

            return null;
        }

        /* @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->deserialize($response, GitHubUserDto::class);
    }

    /**
     * @param array $queryParams ["page" => 1, "per_page" => 10]
     *
     * @return GitHubRepoDto[]
     */
    public function getRepos(string $username, array $queryParams = []): array
    {
        $endpoint = sprintf('users/%s/repos', $username);
        $response = $this->client->request('GET', $endpoint, [
            'query' => $queryParams,
        ]);

        if (Response::HTTP_OK !== $response->getStatusCode()) {
            $this->logging($endpoint, $response);

            return [];
        }

        return $this->deserialize($response, GitHubRepoDto::class.'[]') ?: [];
    }
}

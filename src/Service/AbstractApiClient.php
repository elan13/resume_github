<?php

namespace App\Service;

use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Throwable;

abstract class AbstractApiClient
{
    protected ClientInterface $client;
    protected SerializerInterface $serializer;
    protected ?LoggerInterface $logger = null;

    public function __construct(ClientInterface $client, SerializerInterface $serializer)
    {
        $this->client = $client;
        $this->serializer = $serializer;
    }

    /**
     * @required
     */
    public function setLoggerInterface(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return object|array|null
     */
    protected function deserialize(ResponseInterface $response, string $class)
    {
        try {
            return $this->serializer->deserialize(
                $response->getBody()->getContents(),
                $class,
                JsonEncoder::FORMAT
            );
        } catch (Throwable $e) {
            if ($this->logger) {
                $this->logger->error('{file}:{line} thrown an exception. {cause}', [
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'cause' => $e->getMessage(),
                ]);
            }
        }

        return null;
    }

    protected function logging(string $endpoint, ResponseInterface $response): void
    {
        if (!$this->logger) {
            return;
        }

        $this->logger->error('"{endpoint}". Response returned with {statusCode} "{reasonPhrase}" status code. Body: {body}', [
            'endpoint' => $endpoint,
            'statusCode' => $response->getStatusCode(),
            'reasonPhrase' => $response->getReasonPhrase(),
            'body' => $response->getBody(),
        ]);
    }
}

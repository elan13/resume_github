<?php

namespace App\DataProvider;

interface DataProviderInterface
{
    /**
     * Retrieves an item.
     *
     * @param int|string $id
     */
    public function getItem($id, array $context = []): ?object;
}

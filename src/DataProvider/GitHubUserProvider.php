<?php

namespace App\DataProvider;

use App\Dto\GitHubUserDto;
use App\Service\GitHubApiClient;
use InvalidArgumentException;

class GitHubUserProvider implements DataProviderInterface
{
    private GitHubApiClient $apiClient;

    public function __construct(GitHubApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    /**
     * @return GitHubUserDto|null
     */
    public function getItem($username, array $context = []): ?object
    {
        if (!is_int($username) && !is_string($username) || empty($username)) {
            throw new InvalidArgumentException(sprintf('username must be a string or integer, %s given', gettype($username)));
        }

        return $this->apiClient->getUser((string) $username);
    }
}

<?php

namespace App\DataProvider;

use App\Dto\GitHubLanguage;
use App\Dto\GitHubRepoDto;
use App\Dto\GitHubRepoInfoDto;
use App\Service\GitHubApiClient;
use InvalidArgumentException;

class GitHubRepoInfoProvider implements DataProviderInterface
{
    public const PAGE_DEFAULT = 1;
    public const PER_PAGE_DEFAULT = 100;
    public const FETCH_ALL_REPOS_DEFAULT = true;

    private GitHubApiClient $apiClient;

    public function __construct(GitHubApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    /**
     * @return GitHubRepoInfoDto
     */
    public function getItem($username, array $context = []): ?object
    {
        if (!is_int($username) && !is_string($username) || empty($username)) {
            throw new InvalidArgumentException(sprintf('username must be a string or integer, %s given', gettype($username)));
        }

        // prepare params
        $fetchAllRepos = $context['fetch_all_repos'] ?? static::FETCH_ALL_REPOS_DEFAULT;
        $queryParams = [
            'page' => $context['page'] ?? static::PAGE_DEFAULT,
            'per_page' => $context['per_page'] ?? static::PER_PAGE_DEFAULT,
        ];

        // ApiClient has a maximum per_page = 100; iterate to get all Repos
        $allRepos = [];
        do {
            $repos = $this->apiClient->getRepos($username, $queryParams);
            $allRepos = array_merge($allRepos, $repos);

            // set next page
            ++$queryParams['page'];

            // continues if Repos returned the same amount as per_page; means that more pages could be available
        } while ($fetchAllRepos && count($repos) >= $queryParams['per_page']);

        // sort Repos by popularity
        uasort($allRepos, function (GitHubRepoDto $a, GitHubRepoDto $b) {
            return $b->getPopularity() <=> $a->getPopularity();
        });

        $info = new GitHubRepoInfoDto($username);
        $info->setRepos($allRepos);
        $info->setLanguages($this->calculateLanguagePercentage($allRepos));

        return $info;
    }

    /**
     * calculate Percentage how much code were written in all available Languages
     * writtenCodeSizeInLang*100/totalSize.
     *
     * @param GitHubRepoDto[] $repos
     *
     * @return GitHubLanguage[]
     */
    private function calculateLanguagePercentage(array $repos): array
    {
        $totalSize = 0;

        /** @var GitHubLanguage[] $languages */
        $languages = [];
        foreach ($repos as $repo) {
            if (!($language = $repo->getLanguage())) {
                continue;
            }

            $size = $repo->getSize();

            // calculate total size of written code, when Language available
            $totalSize += $size;

            // create GitHubLanguage, if it doesn't already exist
            if (!isset($languages[$language])) {
                $languageDto = new GitHubLanguage($language);
                $languages[$language] = $languageDto;
            }

            // set setUsageSize; previous UsageSize is equal 0, if it Language is a new object
            $languages[$language]->setUsageSize($languages[$language]->getUsageSize() + $size);
        }

        // calculate percentage and set to GitHubLanguages
        foreach ($languages as $language) {
            $language->setPercent(round($language->getUsageSize() * 100 / $totalSize, 1));
        }

        // sort by percentage
        uasort($languages, function (GitHubLanguage $a, GitHubLanguage $b) {
            return $b->getPercent() <=> $a->getPercent();
        });

        return $languages;
    }
}

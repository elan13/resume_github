<?php

namespace App\Dto;

use DateTimeImmutable;
use Symfony\Component\Serializer\Annotation\SerializedName;

class GitHubRepoDto
{
    private int $id;

    private string $name;

    /**
     * @SerializedName("full_name")
     */
    private ?string $fullName = null;

    private string $url;

    /**
     * @SerializedName("html_url")
     */
    private ?string $htmlUrl = null;

    private ?string $description = null;

    private ?string $language = null;

    private ?string $homepage = null;

    private int $size = 0;

    private int $watchers = 0;

    private int $forks = 0;

    /**
     * @SerializedName("created_at")
     */
    private DateTimeImmutable $createdAt;

    public function __construct(int $id, string $name, string $url, DateTimeImmutable $createdAt)
    {
        $this->id = $id;
        $this->name = $name;
        $this->url = $url;
        $this->createdAt = $createdAt;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(?string $fullName): void
    {
        $this->fullName = $fullName;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getHtmlUrl(): ?string
    {
        return $this->htmlUrl;
    }

    public function setHtmlUrl(?string $htmlUrl): void
    {
        $this->htmlUrl = $htmlUrl;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(?string $language): void
    {
        $this->language = $language;
    }

    public function getHomepage(): ?string
    {
        return $this->homepage;
    }

    public function setHomepage(?string $homepage): void
    {
        $this->homepage = $homepage;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function setSize(int $size): void
    {
        $this->size = $size;
    }

    public function getWatchers(): int
    {
        return $this->watchers;
    }

    public function setWatchers(int $watchers): void
    {
        $this->watchers = $watchers;
    }

    public function getForks(): int
    {
        return $this->forks;
    }

    public function setForks(int $forks): void
    {
        $this->forks = $forks;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getPopularity(): int
    {
        return $this->watchers + $this->forks;
    }
}

<?php

namespace App\Dto;

use DateTimeImmutable;
use Symfony\Component\Serializer\Annotation\SerializedName;

class GitHubUserDto
{
    private int $id;

    /**
     * @SerializedName("login")
     */
    private string $username;

    /**
     * @SerializedName("public_repos")
     */
    private int $publicRepos = 0;

    private int $followers = 0;

    private ?string $url = null;

    /**
     * @SerializedName("html_url")
     */
    private ?string $htmlUrl = null;

    private ?string $name = null;

    private ?string $blog = null;

    private ?string $email = null;

    /**
     * @SerializedName("created_at")
     */
    private DateTimeImmutable $createdAt;

    public function __construct(int $id, string $username, DateTimeImmutable $createdAt)
    {
        $this->id = $id;

        $this->username = $username;

        $this->createdAt = $createdAt;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPublicRepos(): int
    {
        return $this->publicRepos;
    }

    public function setPublicRepos(int $publicRepos): void
    {
        $this->publicRepos = $publicRepos;
    }

    public function getFollowers(): int
    {
        return $this->followers;
    }

    public function setFollowers(int $followers): void
    {
        $this->followers = $followers;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): void
    {
        $this->url = $url;
    }

    public function getHtmlUrl(): ?string
    {
        return $this->htmlUrl;
    }

    public function setHtmlUrl(?string $htmlUrl): void
    {
        $this->htmlUrl = $htmlUrl;
    }

    public function getName(): string
    {
        return $this->name ?: $this->username;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getBlog(): ?string
    {
        return $this->blog;
    }

    public function setBlog(?string $blog): void
    {
        $this->blog = $blog;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }
}

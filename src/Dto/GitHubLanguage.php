<?php

namespace App\Dto;

class GitHubLanguage
{
    private string $languageName;

    /**
     * how many lines of code  were written in that particular language.
     */
    private int $usageSize = 0;

    /**
     * percent of usageSize*100/totalSize.
     */
    private float $percent = 0.0;

    public function __construct(string $languageName)
    {
        $this->languageName = $languageName;
    }

    public function getLanguageName(): string
    {
        return $this->languageName;
    }

    public function getUsageSize(): int
    {
        return $this->usageSize;
    }

    public function setUsageSize(int $usageSize): void
    {
        $this->usageSize = $usageSize;
    }

    public function getPercent(): float
    {
        return $this->percent;
    }

    public function setPercent(float $percent): void
    {
        $this->percent = $percent;
    }
}

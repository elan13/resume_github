<?php

namespace App\Dto;

class GitHubRepoInfoDto
{
    public const MAX_POPULAR_REPOS = 5;

    private string $username;

    /**
     * @var GitHubRepoDto[]
     */
    private array $repos = [];

    /**
     * @var GitHubLanguage[]
     */
    private array $languages = [];

    public function __construct(string $username)
    {
        $this->username = $username;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return GitHubRepoDto[]
     */
    public function getRepos(): array
    {
        return $this->repos;
    }

    /**
     * @param GitHubRepoDto[] $repos
     */
    public function setRepos(array $repos): void
    {
        foreach ($repos as $repo) {
            if (!$repo instanceof GitHubRepoDto) {
                throw new \InvalidArgumentException(sprintf('$repo must be only instance of %s', GitHubRepoDto::class));
            }

            if (!in_array($repo, $this->repos)) {
                $this->repos[] = $repo;
            }
        }
    }

    /**
     * @return GitHubLanguage[]
     */
    public function getLanguages(): array
    {
        return $this->languages;
    }

    /**
     * @param GitHubLanguage[] $languages
     */
    public function setLanguages(array $languages): void
    {
        foreach ($languages as $language) {
            if (!$language instanceof GitHubLanguage) {
                throw new \InvalidArgumentException(sprintf('$language must be only instance of %s', GitHubLanguage::class));
            }

            if (!in_array($language, $this->languages)) {
                $this->languages[] = $language;
            }
        }
    }
}

<?php

namespace App\Controller;

use App\DataProvider\GitHubRepoInfoProvider;
use App\DataProvider\GitHubUserProvider;
use App\Dto\GitHubUserDto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ResumeController extends AbstractController
{
    private GitHubUserProvider $gitHubUserProvider;
    private GitHubRepoInfoProvider $gitHubRepoInfoProvider;

    public function __construct(GitHubUserProvider $gitHubUserProvider, GitHubRepoInfoProvider $gitHubRepoInfoProvider)
    {
        $this->gitHubUserProvider = $gitHubUserProvider;
        $this->gitHubRepoInfoProvider = $gitHubRepoInfoProvider;
    }

    /**
     * @Route("/", name="resume_index")
     */
    public function index(): Response
    {
        return $this->render('resume/index.html.twig');
    }

    /**
     * @Route("/resume/{username}", name="resume_show")
     */
    public function show(string $username): Response
    {
        $user = $this->gitHubUserProvider->getItem($username);
        if (!$user instanceof GitHubUserDto) {
            throw new NotFoundHttpException(sprintf('User with name "%s" not found', $username));
        }

        $repoInfo = $this->gitHubRepoInfoProvider->getItem($username);

        return $this->render('resume/resume.html.twig', [
            'user' => $user,
            'repoInfo' => $repoInfo,
        ]);
    }
}

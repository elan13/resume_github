# Resume Github

- [Installation](#installation)

## Installation

1. Clone the project. 
``````
git clone git@bitbucket.org:elan13/resume_github.git
``````

2. Install all dependencies /  
``````
composer install

or (required extentions can be also ignored)
composer install --ignore-platform-reqs

or (use symfony cli)
symfony composer install --ignore-platform-reqs
``````

3. Serve project to http://127.0.0.1:8000
``````
php bin/console serve:start 
or 
php bin/console serve:run

or (use symfony cli)
symfony server:start
``````

